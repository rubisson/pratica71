/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rubinho
 */
public class Jogador {
    private String nome;
    private Integer numero;
    public Jogador(){
        
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }
    @Override
    public String toString() {
        return "Número: "+numero+ "" +
					"\n"+ "Nome: "+nome+"";
    }    



}
