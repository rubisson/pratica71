/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Pratica71 {
    public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	Jogador j;
	List<Jogador> listaJogador = new ArrayList<>();
	int opcao = 0;
	do {
            System.out.println("## Escolha uma das opções abaixo ##");
            System.out.println("Opção 1 - Cadastra Jogador");
	    System.out.println("Opção 2 - Imprime Jogador cadastradas");
	    System.out.println("Opção 0 - Sair do programa");
	    System.out.println("_______________________");
	        
	    System.out.print("Digite aqui sua opção: ");
	    opcao = Integer.parseInt(sc.nextLine());
	    if(opcao == 1){
                //Cria um novo objeto 
	        j = new Jogador();
	    	System.out.print("Digite o número: ");
	    	j.setNumero(Integer.parseInt(sc.nextLine()));
	    	System.out.print("Digite o nome: ");
	    	j.setNome(sc.nextLine());
	    	//Guarda o objeto pessoa em uma lista.
	    	listaJogador.add(j);
	    }else if(opcao == 2){
                if(listaJogador.isEmpty()){
                    System.out.println("Não existem pessoas cadastradas, pressione uma tecla para continuar!");
                    sc.nextLine();
	        }else{
                    System.out.println(listaJogador.toString());
                    System.out.println("Pressione um tecla para continuar.");
		    sc.nextLine();
                    }
                }
	} while (opcao != 0);
	sc.close();
    }
} 
